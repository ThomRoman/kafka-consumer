package com.consumermongodbkafka.consumermongodbkafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;

@EnableKafka
@SpringBootApplication
public class ConsumermongodbkafkaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsumermongodbkafkaApplication.class, args);
	}

}
