package com.consumermongodbkafka.consumermongodbkafka.service;

import com.consumermongodbkafka.consumermongodbkafka.model.ProductEvent;
import com.consumermongodbkafka.consumermongodbkafka.repository.ProductEventRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.type.TypeReference;
import java.time.Instant;
import java.util.Map;

@Slf4j
@Service
public class TopicListener {
	@Autowired
	private ProductEventRepository productEventRepository;

	@Value("${topic.name.consumer}")
	private String topicName;

	@KafkaListener(topics = "${topic.name.consumer}",groupId = "group_id")
	public void consume(ConsumerRecord<String, String> payload){
		ObjectMapper objectMapper = new ObjectMapper();
		try {
//			System.out.println(payload.value());
			Map<String, Object> payloadMap = objectMapper.readValue(payload.value(), new TypeReference<Map<String, Object>>(){});
			ProductEvent productEvent = new ProductEvent();
			productEvent.setEventType(payloadMap.get("eventType").toString());
			productEvent.setTimestamp(payloadMap.get("timestamp").toString());
			productEvent.setProductId(payloadMap.get("productId").toString());
			productEventRepository.save(productEvent);
		} catch (Exception e) {
			log.error("Error al procesar el payload: {}", e.getMessage(), e);
		}
	}
}
