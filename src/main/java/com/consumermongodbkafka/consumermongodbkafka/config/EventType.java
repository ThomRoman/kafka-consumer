package com.consumermongodbkafka.consumermongodbkafka.config;

public enum EventType {
	PRODUCT_VIEW,
	PRODUCT_ADDED_TO_CART,
	PRODUCT_PURCHASED
}
