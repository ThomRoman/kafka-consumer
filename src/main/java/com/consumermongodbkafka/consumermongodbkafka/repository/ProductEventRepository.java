package com.consumermongodbkafka.consumermongodbkafka.repository;

import com.consumermongodbkafka.consumermongodbkafka.model.ProductEvent;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductEventRepository extends MongoRepository<ProductEvent,String> {
}
