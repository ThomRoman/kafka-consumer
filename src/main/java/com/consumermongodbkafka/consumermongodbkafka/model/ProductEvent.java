package com.consumermongodbkafka.consumermongodbkafka.model;

import com.consumermongodbkafka.consumermongodbkafka.config.EventType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Setter
@Getter
@RequiredArgsConstructor
@AllArgsConstructor
@Document(collection = "productsEvents")
public class ProductEvent {
	@Id
	private String id;
	private String productId;
	private String eventType;
	private String timestamp;
}
